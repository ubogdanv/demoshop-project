package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CartPage {
    private final SelenideElement greetingsMessage = $(".text-center");
    private final ElementsCollection products = $$(".row .row");
    private final List<ProductInCart> productsInCart = new ArrayList<>();
    private final SelenideElement continueToCheckout = $(".btn-success");
    private final SelenideElement totalAmount = $(".amount-total");


    public void withProduct(Product p) {
        ProductInCart pic = new ProductInCart(p);
        productsInCart.add(pic);
    }

    public boolean clickToContinueToCheckout() {
        continueToCheckout.click();
        return true;
    }

    public String getGreetingsMessage() {
        return greetingsMessage.getText();
    }

    public String getNumberOfProducts() {
        return String.valueOf(products.size());
    }


    public String getTotalAmount() {
        return totalAmount.text().replaceAll("\\$", "");
    }

    public List<ProductInCart> getProductsInCart() {
        return productsInCart;
    }

}




