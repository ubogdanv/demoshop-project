package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class OrderSummaryPage extends CheckoutInfoPage {
    private final SelenideElement summaryPageName = $(".subheader-container small");
    private final SelenideElement cancelButton = $(".btn-danger");

    private final SelenideElement completeButton = $("[href='#/checkout-complete']");

    private final SelenideElement totalOrderAmount = $(".amount-total .amount");
    private final SelenideElement backToShoppingButton = $("[href='#/products']");
    private final SelenideElement confirmOrderMsg = $(".text-center");

    public void openOrderSummaryPage() {
        String ORDERSUMMARY_URL = "https://fasttrackit-test.netlify.app/#/checkout-summary";
        System.out.println("Open " + ORDERSUMMARY_URL);
        open(ORDERSUMMARY_URL);
    }

    public boolean isSummaryPageNameDisplayed() {
        return this.summaryPageName.exists() && this.summaryPageName.isDisplayed();
    }

    public String getSummaryPageName() {
        return this.summaryPageName.text();
    }

    public boolean returnToCartPage_FromOrderSummaryPage() {
        cancelButton.click();
        return true;
    }

    public void clickOnCompleteButton() {
        completeButton.click();
    }

    public String getTotalOrderAmount() {
        return totalOrderAmount.text().replaceAll("\\$", "");
    }

    public boolean clickOnBackToShoppingButton() {
        backToShoppingButton.click();
        return true;
    }

    public String getConfirmOrderMsg() {
        return this.confirmOrderMsg.text();
    }
}

