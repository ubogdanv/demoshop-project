package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Attachment;

import static com.codeborne.selenide.Selenide.*;


public class Page {
    public static final String HOMEPAGE_URL = "https://fasttrackit-test.netlify.app/#/";

    private final ElementsCollection products = $$(".card");

    private final SelenideElement modalDialog = $(".modal-dialog");
    private final SelenideElement subPageTitle = $(".text-muted");
    private final SelenideElement searchField = $("#input-search");
    private final SelenideElement searchButton = $(".btn-light");


    public void openPage() {
        System.out.println("Open " + HOMEPAGE_URL);
        open(HOMEPAGE_URL);
    }

    @Attachment
    public boolean isModalDisplayed() {
        return modalDialog.exists() && modalDialog.isDisplayed();
    }

    @Attachment
    public String getTitle() {
        return Selenide.title();
    }
    public  void clickOnSearchButton() {
        searchButton.click();
    }

    public boolean clickOnSearchField() {
        searchField.click();
        return true;
    }

    public void fillInSearchField(String word) {
        searchField.sendKeys(word);
    }


    public String getSubPageTitle() {
        return this.subPageTitle.text();
    }

    public ElementsCollection getProducts() {
        return products;
    }
}







