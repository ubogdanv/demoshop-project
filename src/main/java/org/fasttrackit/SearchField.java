package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class SearchField {
    private final SelenideElement searchField = $("#input-search");
    private final SelenideElement searchButton = $(".btn-light");


    public boolean isSearchButtonDisplayed() {
        return this.searchButton.exists() && this.searchButton.isDisplayed();
    }

    public boolean isSearchButtonClickable(){
        searchButton.click();
        return true;
    }
    public boolean clickOnSearchField() {
        searchField.click();
        return true;
    }
    public boolean isSearchFieldDisplayed() {
        return this.searchField.exists() && this.searchField.isDisplayed();
    }

}
