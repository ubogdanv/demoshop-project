package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutInfoPage {

    private final SelenideElement pageName = $(".text-muted");
    private final SelenideElement firstNameField = $("#first-name");
    private final SelenideElement lastNameField = $("#last-name");
    private final SelenideElement addressField = $("#address");
    private final SelenideElement continueCheckoutButton = $(".btn-success");
    private final SelenideElement errorMsg = $(".error");


    public void typeInFirstNameField(String FirstNameToType) {
        this.firstNameField.click();
        this.firstNameField.sendKeys(FirstNameToType);
    }

    public void typeInSecondNameField(String LastNameToType) {
        this.lastNameField.click();
        this.lastNameField.sendKeys(LastNameToType);
    }

    public void typeInAddressField(String AddressToType) {
        this.addressField.click();
        this.addressField.sendKeys(AddressToType);
    }


    public void clickOnContinueCheckoutButton() {
        this.continueCheckoutButton.click();
    }

    public boolean firstNameFieldExistAndIsDisplayed() {
        return this.firstNameField.exists() && this.firstNameField.isDisplayed();
    }

    public boolean secondNameFieldExistAndIsDisplayed() {
        return this.lastNameField.exists() && this.lastNameField.isDisplayed();
    }

    public boolean addressFieldExistAndIsDisplayed() {
        return this.addressField.exists() && this.addressField.isDisplayed();
    }

    public String getPageName() {
        return pageName.getText();
    }

    public String getErrorMsg() {
        return errorMsg.getText();
    }
}


