package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {

    private final SelenideElement cartCounter = $(".shopping_cart_badge");
    private final SelenideElement loginIcon = $(".fa-sign-in-alt");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement homeIcon = $(".fa-shopping-bag");
    private final SelenideElement wishListIcon = $("[href='#/wishlist']");
    private final SelenideElement wishlistBadge = $(".shopping_cart_badge");


    public String getGreetingsMsg() {
        SelenideElement greetingsMessage = $(".navbar-text span span");
        return greetingsMessage.text();
    }

    public void clickOnTheLoginIcon() {
        loginIcon.click();
        System.out.println("Click on the " + loginIcon + " from header");
    }

    public void clickOnTheCartIcon() {
        cartIcon.click();
    }

    public void clickOnHomeIcon() {
        homeIcon.click();

    }

    public void clickOnTheWishlistIcon() {
        wishListIcon.click();
    }

    public String getCartCounter () {
        return cartCounter.getText();
        }
        public String getNumberOfProductsInWishlist () {
        return this.wishlistBadge.text();
            }

    }
