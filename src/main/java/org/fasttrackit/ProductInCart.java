package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ProductInCart {

    private final Product product;
    private final SelenideElement link;
    private  final  SelenideElement parentRow;
    private final String name;
    private final String unitPrice;
    private final String totalProductPrice;
    private final String quantity;
    private final SelenideElement decrement;
    private final SelenideElement increment;

    private final SelenideElement deleteFromCart;
    private final SelenideElement totalAmount;

    private final SelenideElement checkout;


    public ProductInCart(Product product) {
        this.product = product;
        String productId = String.format("#item_%s_title_link", product.getProductId());
        this.link = $(productId);
        this.name = link.getText();
        this.parentRow = link.parent().parent();
        this.increment = parentRow.$(".fa-plus-circle");
        this.decrement = parentRow.$(".fa-minus-circle");
        ElementsCollection columns = this.parentRow.$$(".col-md-auto");
        this.quantity = columns.first().getText();
        this.unitPrice = columns.get(1).getText();
        this.totalProductPrice = columns.get(2).getText();
        this.deleteFromCart = this.parentRow.$(".fa-trash");
        this.checkout = $(".btn-success");
        this.totalAmount = $(".amount-total");
    }

    public void clickOnTheDeleteFromCart() {
        deleteFromCart.click();
    }
    public void clickOnCheckout() {
        checkout.click();
    }
    public String getCount() {
        return quantity;
    }
    public void increaseAmount() {
        increment.click();
    }

    public void reduceAmount() {
        decrement.click();
    }

    public SelenideElement getTotalAmount() {
        return totalAmount;
    }
}
