package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import org.fasttrackit.config.TestConfiguration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class AddProductsToCartTest extends TestConfiguration {
    Page demoShopPage;
    Header header;
    Footer footer;

    @BeforeTest
    private void setup() {
        demoShopPage = new Page();
        demoShopPage.openPage();
        header = new Header();
        footer = new Footer();
    }

    @AfterMethod
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = "When adding a product to Cart, the cart Badge is incremented", testName = "Add Products to Cart", priority = 1, suiteName = "Products in Cart")
    public void whenAddingAProductInCart_CartBadgeIsIncremented() {
        Product p = new Product("1", "Awesome Granite Chips", "15.99");
        p.addToCart();
        assertEquals(header.getCartCounter(), "1", "When adding a product in cart, cart badge is incremented by 1");

    }

    @Test(description = "When adding identical products to Cart, the cart Badge is incremented", testName = "Add Products to Cart", priority = 1, suiteName = "Products in Cart")
    public void whenAddingIdenticalProductsInCart_CartBadgeIsIncrementedByTwo() {
        Product p = new Product("1", "Awesome Granite Chips", "15.99");
        p.addToCart();
        p.addToCart();
        assertEquals(header.getCartCounter(), "2", "When adding two identical products in cart, cart badge is incremented by 2");

    }

    @Test(description = "When adding a two different products to Cart, the cart Badge is incremented", testName = "Add Products to Cart", priority = 1, suiteName = "Products in Cart")
    public void whenAddingTwoDifferentProductsInCart_CartBadgeIsIncrementedByTwo() {
        Product p1 = new Product("1", "Awesome Granite Chips", "15.99");
        Product p2 = new Product("2", "Incredible Concrete Hat", "7.99");
        p1.addToCart();
        p2.addToCart();
        assertEquals(header.getCartCounter(), "2", "When adding two different products in cart, cart badge is incremented by 2");

    }
}
