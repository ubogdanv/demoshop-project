package org.fasttrackit;


import com.codeborne.selenide.Selenide;
import org.fasttrackit.config.TestConfiguration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class SearchFieldTest extends TestConfiguration {
    Page demoShopPage;
    Header header;
    Footer footer;
    SearchField searchField;


    @BeforeTest
    public void setup() {
        demoShopPage = new Page();
        demoShopPage.openPage();
        searchField = new SearchField();
        header = new Header();
        footer = new Footer();

    }

    @AfterMethod
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test
    public void verifyIfSearchFieldIsDisplayed() {
        assertTrue(searchField.isSearchFieldDisplayed(), "The search field must be displayed on homepage");
    }

    @Test
    public void verifyIfSearchButtonIsDisplayed() {
        assertTrue(searchField.isSearchButtonDisplayed(), "Search button must be displayed on homepage");
    }

    @Test
    public void verifyIfSearchFieldIsClickable() {
        assertTrue(searchField.clickOnSearchField(),"Search field from homepage must be clickable" );
    }@Test

    public void verifyIfSearchButtonIsClickable() {
        assertTrue(searchField.isSearchButtonClickable(),"Search button from homepage must be clickable" );
    }
}

