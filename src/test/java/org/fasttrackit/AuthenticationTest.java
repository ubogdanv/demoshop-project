package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.DataProviderForDemoshop;
import org.fasttrackit.dataprovider.InvalidUser;
import org.fasttrackit.dataprovider.User;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static org.testng.Assert.*;


public class AuthenticationTest extends TestConfiguration {

    Page demoShopPage;
    Header header;
    Footer footer;


    @BeforeTest
    @Step("Reset Page")
    public void setup() {
        demoShopPage = new Page();
        demoShopPage.openPage();
        header = new Header();
        footer = new Footer();

    }

    @AfterMethod
    @Step("Reset Page")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test
    @Link(value = "https://bugTracker.org/bug/1234Fasttrackit", name = "1234Fasttrackit")
    @Severity(SeverityLevel.BLOCKER)
    @Story("Modal is Displayed when clicking on the login icon.")
    public void whenClickingOnLoginButton_LoginModalIsOpened() {
        header.clickOnTheLoginIcon();
        boolean modalDisplayed = demoShopPage.isModalDisplayed();
        assertTrue(modalDisplayed, "When clicking on the login button, Login modal is open");

    }

    @Severity(SeverityLevel.CRITICAL)
    @Test(dataProviderClass = DataProviderForDemoshop.class, dataProvider = "UserDataProvider")
    public void whenLoginWithValidCredentials_GreetingsMessageIsDisplayed(User user) {
        header.clickOnTheLoginIcon();
        Modal loginModal = new Modal();
        loginModal.clickOnTheUsernameField();
        loginModal.typeInUserName(user.getUserName());
        loginModal.clickOnThePasswordField();
        loginModal.typeInPassword(user.getPassword());
        loginModal.clickOnTheLoginButton();
        String greetingsMsg = header.getGreetingsMsg();
        assertEquals(greetingsMsg, user.getGreetingsMessage(), String.format("When login with  valid user  greetings message shows Hi %s!", user.getUserName()));

    }

    @Severity(SeverityLevel.MINOR)
    @Test(dataProviderClass = DataProviderForDemoshop.class, dataProvider = "InvalidUserDataProvider")
    public void whenLoginWithInvalidCredentials_GreetingsMessageIsDisplayedAsGuest(InvalidUser user) {
        header.clickOnTheLoginIcon();
        Modal loginModal = new Modal();
        loginModal.clickOnTheUsernameField();
        loginModal.typeInUserName(user.getUserName());
        loginModal.clickOnThePasswordField();
        loginModal.typeInPassword(user.getPassword());
        loginModal.clickOnTheLoginButton();
        String greetingsMsg = header.getGreetingsMsg();
        assertEquals(greetingsMsg, user.getGreetingsMessage(), String.format("When login with  invalid user  guest greetings message shows Hi %s!", user.getUserName()));
        assertEquals(loginModal.getErrorMsg(),user.getErrorValidationMsg());

    }

}














