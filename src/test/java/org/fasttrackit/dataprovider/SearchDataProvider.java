package org.fasttrackit.dataprovider;

import org.testng.annotations.DataProvider;

public class SearchDataProvider {
    @DataProvider(name = "searchWordDataProvider")
    public Object[][] feedWordDataProvider() {
        Search mouse = new Search("mouse");
        Search metal = new Search("metal");
        Search bacon = new Search("bacon");
        Search awesome = new Search("awesome");
        Search shirt = new Search("shirt");
        Search pizza = new Search("pizza");

        return new Object[][]{
                {mouse},
                {metal},
                {bacon},
                {awesome},
                {shirt},
                {pizza}
        };
    }

    @DataProvider(name = "invalidWordDataProvider")
    public Object[][] feedInvalidWordDataProvider() {
        Search type = new Search("type");
        Search order_number = new Search("order number");
        Search dimension = new Search("dimension");


        return new Object[][]{
                {type},
                {order_number},
                {dimension}
        };
    }
}

