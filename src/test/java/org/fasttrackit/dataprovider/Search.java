package org.fasttrackit.dataprovider;

public class Search {
    private final String searchWord;

    public Search(String searchWord) {
        this.searchWord = searchWord;
    }

    public String getSearchWord() {
        return searchWord;
    }
    @Override
    public String toString() {
        return "Test run with: " + searchWord;
    }
}

