package org.fasttrackit.dataprovider;

public class User {

    private  final String userName;
    private  final String password;
    private final String greetingsMessage;

    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
        this.greetingsMessage = String.format("Hi %s!", userName);

    }

    public String getUserName() {
        return userName;
    }
    public String getPassword() {
        return password;
    }
     public String getGreetingsMessage() {
        return greetingsMessage;
     }

    @Override
    public String toString() {
        return userName;
    }
}
