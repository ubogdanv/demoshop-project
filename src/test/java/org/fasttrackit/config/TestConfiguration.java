package org.fasttrackit.config;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.testng.ScreenShooter;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;

public class TestConfiguration {
    public TestConfiguration() {
        Configuration.browser = "Chrome";
        ScreenShooter.captureSuccessfulTests = true;
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-fullscreen");
        Configuration.browserCapabilities = options;
    }

    @AfterMethod
    public void tearDown() {
        screenshot();
    }

    @Attachment(type = "image/png")
    public byte[] screenshot() {
        return Selenide.screenshot(OutputType.BYTES);
    }

}

