package org.fasttrackit;


import org.fasttrackit.config.TestConfiguration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class OrderSummaryTest extends TestConfiguration {

    Header header;
    Footer footer;
    OrderSummaryPage orderSummaryPage;


    @BeforeMethod
    private void setup() {
        header = new Header();
        footer = new Footer();
        orderSummaryPage = new OrderSummaryPage();
        orderSummaryPage.openOrderSummaryPage();
    }

    @AfterMethod
    public void cleanUp() {
        footer.resetPage();
        header.clickOnHomeIcon();
    }

    @Test
    public  void summaryOrderPageNameIsDisplayed() {
        assertTrue(orderSummaryPage.isSummaryPageNameDisplayed(),"Order summary page name must be displayed");
    }

    @Test
    public void pageNameIsAccordingly() {
        assertEquals(orderSummaryPage.getSummaryPageName(), "Order summary", "This title must appear: Order summary");
    }

    @Test
    public void whenClickingOnCancelButton_CartPageIsOpen() {
        assertTrue(orderSummaryPage.returnToCartPage_FromOrderSummaryPage(), "When clicking on cancel button, cart page must open");
    }

    @Test
    public void totalOrderAmountIsDisplayedAndIsCorrect() {
        assertEquals(orderSummaryPage.getTotalOrderAmount(), "0", "The total amount must be correct");
    }

    @Test
    public void whenClickingOnCompleteOrder_ConfirmOrderMsgIsDisplayed() {
        orderSummaryPage.clickOnCompleteButton();
        assertEquals(orderSummaryPage.getConfirmOrderMsg(), "Thank you for your order!", "When complete order is done, confirm order message is displayed");
    }

    @Test

    public void whenClickingOnBackToShoppingButtonFromOrderComplete_CartPageIsOpen(){
        orderSummaryPage.clickOnCompleteButton();
        assertTrue(orderSummaryPage.clickOnBackToShoppingButton(),"When clicking on continue shopping button, home page is opened");
    }
}
