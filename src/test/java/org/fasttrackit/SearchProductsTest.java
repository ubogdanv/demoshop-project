package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.Search;
import org.fasttrackit.dataprovider.SearchDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class SearchProductsTest extends TestConfiguration {
    Page demoShopPage;
    Header header;
    Footer footer;


    @BeforeMethod
    public void setup() {
        demoShopPage = new Page();
        demoShopPage.openPage();
        header = new Header();
        footer = new Footer();
    }

    @AfterMethod
    @Step("Reset Page")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = " Search valid word in the product list",
            suiteName = "Search products",
            dataProviderClass = SearchDataProvider.class, dataProvider = "searchWordDataProvider")
    @Severity(SeverityLevel.NORMAL)
    public void searchValidWordInTheProductList(Search word) {
        demoShopPage.clickOnSearchField();
        demoShopPage.fillInSearchField(word.getSearchWord());
        demoShopPage.clickOnSearchButton();
        ElementsCollection products = demoShopPage.getProducts();
        for (SelenideElement product : products) {
            assertTrue(product.text().toLowerCase().contains(word.getSearchWord().toLowerCase()));
        }
    }

    @Test(description = "Search invalid word in the product list",
            suiteName = "Search products",
            dataProviderClass = SearchDataProvider.class, dataProvider = "invalidWordDataProvider")
    @Severity(SeverityLevel.NORMAL)
    public void searchInvalidWordFromProductList(Search word) {
        demoShopPage.clickOnSearchField();
        demoShopPage.fillInSearchField(word.getSearchWord());
        demoShopPage.clickOnSearchButton();
        ElementsCollection products = demoShopPage.getProducts();
        assertTrue(products.isEmpty());
    }
}


