package org.fasttrackit;

import org.fasttrackit.config.TestConfiguration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CheckoutPageTest extends TestConfiguration {
    Page demoShopPage;
    Header header;
    Footer footer;
    Product product;
    CartPage cartPage;
    CheckoutInfoPage checkoutInfoPage;

    @BeforeMethod
    private void setup() {
        demoShopPage = new Page();
        demoShopPage.openPage();
        cartPage = new CartPage();
        header = new Header();
        footer = new Footer();
        checkoutInfoPage = new CheckoutInfoPage();
        product = new Product("1", "Awesome Granite Chips", "15.99");
        product.addToCart();
        header.clickOnTheCartIcon();
        cartPage.clickToContinueToCheckout();
    }

    @AfterMethod
    public void cleanUp() {
        footer.resetPage();
        header.clickOnHomeIcon();
    }

    @Test
    public void pageNameIsAccordingly() {
        assertEquals(checkoutInfoPage.getPageName(), "Your information", "Your information");
    }

    @Test
    public void infoElementIsDisplayed() {
        assertTrue(checkoutInfoPage.firstNameFieldExistAndIsDisplayed(), "First name field must be displayed");
        assertTrue(checkoutInfoPage.secondNameFieldExistAndIsDisplayed(), "Second name field must be displayed");
        assertTrue(checkoutInfoPage.addressFieldExistAndIsDisplayed(), "Address field must be displayed");
    }

    @Test
    public void checkoutIsPossibleAfterCompleteAddressInfo() {
        checkoutInfoPage.typeInFirstNameField("Ungur");
        checkoutInfoPage.typeInSecondNameField("Bogdan");
        checkoutInfoPage.typeInAddressField("Cehu Silvaniei, str. Barrok Bela, nr,23");
        checkoutInfoPage.clickOnContinueCheckoutButton();
        assertEquals(demoShopPage.getSubPageTitle(), "Order summary", "Order summary");
    }

    @Test
    public void cannotContinueToCheckout_IfYourInformationFieldAreBlank() {
        checkoutInfoPage.clickOnContinueCheckoutButton();
        assertEquals(checkoutInfoPage.getErrorMsg(), "Please complete your information!", " First Name is required");

    }

    @Test
    public void cannotContinueToCheckout_IfFirstNameFieldIsBlank() {
        checkoutInfoPage.typeInFirstNameField("");
        checkoutInfoPage.typeInSecondNameField("Bogdan");
        checkoutInfoPage.typeInAddressField("Cehu Silvaniei, str. Barrok Bela, nr,23");
        checkoutInfoPage.clickOnContinueCheckoutButton();
        assertEquals(checkoutInfoPage.getErrorMsg(), "First Name is required", "First Name is required");

    }

    @Test
    public void cannotContinueToCheckout_IfLastNameFieldIsBlank() {
        checkoutInfoPage.typeInFirstNameField("Ungur");
        checkoutInfoPage.typeInSecondNameField("");
        checkoutInfoPage.typeInAddressField("Cehu Silvaniei, str. Barrok Bela, nr,23");
        checkoutInfoPage.clickOnContinueCheckoutButton();
        assertEquals(checkoutInfoPage.getErrorMsg(), "Last Name is required", "Last Name is required");

    }

    @Test
    public void cannotContinueToCheckout_IfAddressFieldIsBlank() {
        checkoutInfoPage.typeInFirstNameField("Ungur");
        checkoutInfoPage.typeInSecondNameField("Bogdan");
        checkoutInfoPage.typeInAddressField("");
        checkoutInfoPage.clickOnContinueCheckoutButton();
        assertEquals(checkoutInfoPage.getErrorMsg(), "Address is required", "Address is required");

    }
}

