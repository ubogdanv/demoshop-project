package org.fasttrackit;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.fasttrackit.config.TestConfiguration;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertTrue;

public class SortDropMenuTest extends TestConfiguration {

Page demoShopPage;
SortDropMenu sortDropMenu;

@BeforeTest
    private void setup() {
    demoShopPage = new Page();
    demoShopPage.openPage();
    sortDropMenu = new SortDropMenu();
    sortDropMenu.clickOnTheSortField();
    sleep(2000);

}


    @Test
    @Severity(SeverityLevel.MINOR)
    public void verifyIfSortAToZIsDisplayed_InSortDropMenu() {
        assertTrue(sortDropMenu.validateSortAToZIsDisplayed(), "Expected Sort by name (A to Z) option to be displayed.");
}
    @Test
    @Severity(SeverityLevel.MINOR)
    public void verifyIfSortZToAIsDisplayed_InSortDropMenu(){
        assertTrue(sortDropMenu.validateSortZToAIsDisplayed(), "Expected Sort by name (A to Z) option to be displayed.");
}
    @Test
    @Severity(SeverityLevel.MINOR)
    public void verifyIfPriceSortLowToHighIsDisplayed_InSortDropMenu(){
        assertTrue(sortDropMenu.validateSortByPriceLowToHighIsDisplayed(), "Expected Sort by price (Low to High) option to be displayed.");
}
    @Test
    @Severity(SeverityLevel.MINOR)
    public void verifyIfPriceSortHighToLowIsDisplayed_InSortDropMenu(){
        assertTrue(sortDropMenu.validateSortByPriceHighToLowIsDisplayed(), "Expected Sort by price (High to Low) option to be displayed.");


    }
}

