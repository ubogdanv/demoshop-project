package org.fasttrackit;

import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.DataProviderForDemoshop;
import org.fasttrackit.dataprovider.User;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class DataProviderForDemoShopTest extends TestConfiguration {
    @Test(dataProvider = "UserDataProvider", dataProviderClass = DataProviderForDemoshop.class)
    public void testMyDataProvider(User user) {
        assertTrue(user.getGreetingsMessage().contains(user.getUserName()), "Expected Name to be " + user.getGreetingsMessage());
    }

}

