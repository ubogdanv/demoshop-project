package org.fasttrackit;

import org.fasttrackit.config.TestConfiguration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class CartPageTest extends TestConfiguration {

    Page demoShopPage;
    CartPage cartPage;
    Header header;
    Footer footer;

    @BeforeTest
    private void setup() {
        demoShopPage = new Page();
        demoShopPage.openPage();
        cartPage = new CartPage();
        header = new Header();
        footer = new Footer();
    }

    @AfterMethod
    public void cleanUp() {
        footer.resetPage();
        header.clickOnHomeIcon();
    }

    @Test
    public void whenCartPageIsEmptyGreetingMessageIsDisplayed() {
        header.clickOnTheCartIcon();
        assertEquals(cartPage.getGreetingsMessage(), "How about adding some products in your cart?",
                "When empty cart page is open greetings message must be displayed");
    }

    @Test
    public void deletProductFromCartTest() {
        Product p = new Product("1", "Awesome Granite Chips", "15.99");
        p.addToCart();
        assertEquals(header.getCartCounter(), "1");
        header.clickOnTheCartIcon();
        cartPage.withProduct(p);

        cartPage.getProductsInCart().get(0).clickOnTheDeleteFromCart();
        assertEquals(cartPage.getGreetingsMessage(), "How about adding some products in your cart?",
                "When empty cart page is open greetings message must be displayed");
    }

    @Test
    public void increaseAmountOfTheSameProductTest() {
        Product p = new Product("1", "Awesome Granite Chips", "15.99");
        p.addToCart();
        header.clickOnTheCartIcon();
        cartPage.withProduct(p);

        cartPage.getProductsInCart().get(0).increaseAmount();
        assertEquals(header.getCartCounter(), "2", "When increase amount of the same product, cart counter must increase by one");
        assertEquals(cartPage.getNumberOfProducts(), "1", "When increase amount of the same product, number of products must be one");
    }

    @Test
    public void increaseAmountOfTwoDifferentProductsTest() {
        Product p = new Product("1", "Awesome Granite Chips", "15.99");
        Product p2 = new Product("2", "Incredible Concrete Hat", "7.99");
        p.addToCart();
        p2.addToCart();
        header.clickOnTheCartIcon();
        cartPage.withProduct(p);
        cartPage.withProduct(p2);

        cartPage.getProductsInCart().get(0).increaseAmount();
        assertEquals(header.getCartCounter(), "3", "When increase amount of two different products, car counter must increase by one");
        assertEquals(cartPage.getNumberOfProducts(), "2", "When increase amount of two different products, number of products must be two");
    }

    @Test
    public void reduceAmountOfProductsTest() {
        Product p = new Product("1", "Awesome Granite Chips", "15.99");
        p.addToCart();
        p.addToCart();
        p.addToCart();
        header.clickOnTheCartIcon();
        cartPage.withProduct(p);


        cartPage.getProductsInCart().get(0).reduceAmount();
        assertEquals(header.getCartCounter(), "2", "When reduce amount of the same product, cart counter must reduce by one");
        assertEquals(cartPage.getNumberOfProducts(), "1", "When reduce amount of the same product, number of product must be one ");
    }

    @Test
    public void reduceMultiplesProductsTest() {
        Product p = new Product("1", "Awesome Granite Chips", "15.99");
        p.addToCart();
        p.addToCart();
        p.addToCart();
        p.addToCart();
        header.clickOnTheCartIcon();
        cartPage.withProduct(p);

        cartPage.getProductsInCart().get(0).reduceAmount();
        cartPage.getProductsInCart().get(0).reduceAmount();
        assertEquals(header.getCartCounter(), "2", "When reduce amount of the same product, cart counter must reduce by one by one");
        assertEquals(cartPage.getNumberOfProducts(), "1", "When reduce amount of the same product, number of product must be one ");
    }

    @Test
    public void goToCheckoutPageTest() {
        Product p = new Product("1", "Awesome Granite Chips", "15.99");
        p.addToCart();
        header.clickOnTheCartIcon();
        cartPage.clickToContinueToCheckout();
        assertTrue(cartPage.clickToContinueToCheckout(), "When clicking on checkout button, Info Checkout page  is opened");
    }

    @Test
    public void calculateTheTotalAmountOfTwoDifferentProducts() {
        Product p1 = new Product("1", "Awesome Granite Chips", "15.99");
        Product p2 = new Product("3", "Awesome Metal Chair", "15.99");
        p1.addToCart();
        p2.addToCart();
        header.clickOnTheCartIcon();
        assertEquals(cartPage.getTotalAmount(), "45.98", "The sum of two or multiples product must be correctly calculate");
    }

    @Test
    public void resetCartPage() {
        footer.resetPage();
        assertTrue(footer.resetPage(), "The Cart Page must be reseted");
    }

}



