# Final project for Testing FastTrackIt Web-Shop Application

### Student: Ungur Bogdan

### Description
Project is designed for testing DemoShop application

Link:https://fasttrackit-test.netlify.app/#/

### Developing stack
- Java
- Maven
- Selenide
- TestNg
- Allure


### Testing application functionalities and features

- Add to cart
- Authentication
- Shopping cart
- Checkout
- Search function
- Sort menu
- Home page
- Order summary

### Data providers
 - User data provider
 - Search data provider

### Run test from Maven command line

- Execute all tests
  - mvn clean test
- Generate report
  - mvn allure:report
- Open and present report
  - mvn allure:serve
  

## Allure report
 - test cases - 54
    - passed - 52
    - failed - 2


#### _Project Link:https://gitlab.com/ubogdanv/demoshop-project.git_



